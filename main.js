class ParteUno extends HTMLElement{

    constructor(){
        const tpl = document.querySelector('#template1');
        const tplInst = tpl.content.cloneNode(true);

        super();
        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(tplInst);

    } 
}
customElements.define('parte-uno', ParteUno);

class ParteDos extends HTMLElement{

    constructor(){
        const tpl = document.querySelector('#template2');
        const tplInst = tpl.content.cloneNode(true);

        super();
        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(tplInst);

    } 
}
customElements.define('parte-dos', ParteDos)


const url = "http://dummy.restapiexample.com/api/v1/employees"
//var tabla = document.querySelector('#tabla_empleados');
var json_data;
var nombre_c;

var tabla= document.querySelector('parte-dos').shadowRoot.querySelector('#tabla_empleados');

//var input_ = document.querySelector('parte-1').shadowRoot.querySelector('#texto_buscado');


function pintarEncabezadoTabla(){
    const row = document.createElement('tr');
        row.innerHTML += `
        <th scope= "col">Name</th>
        <th scope= "col">Lastname</th>
        <th scope= "col">Amount (YEN)</th>`;
        tabla.appendChild(row); 
}

function pintarTabla(element){
    nombre_c = element.employee_name.split(" ");
    const row = document.createElement('tr');
    row.innerHTML += `
        <td>${nombre_c[0]} </td> 
        <td>${nombre_c[1]} </td> 
        <td>${element.employee_salary} </td> `;
        tabla.appendChild(row);    
}

function obtenerDatos(){                
    fetch(url)
    .then(response => response.json())
    .then(empleados => {
        let emp = empleados.data;
        json_data = empleados.data;
        pintarEncabezadoTabla();
        emp.forEach(element => {
            pintarTabla(element);
               
        });
                   
    })
    .catch(err => console.log(err))   
}

function limpiar(){
    borrar_tabla();
    document.querySelector('parte-uno').shadowRoot.querySelector('#texto_buscado').value = "";

    obtenerDatos();
}

function borrar_tabla(){
    tabla.innerHTML= "";
}

function filtrarEmpleados(){
    borrar_tabla();
    pintarEncabezadoTabla();
    //var filtroName = document.getElementById("texto_buscado").value;
    var filtroName = document.querySelector('parte-uno').shadowRoot.querySelector('#texto_buscado');
    filtroOK = filtroName.value;
    console.log(filtroOK);
    

    let filtro = json_data.filter((empleado)=>{
        if( empleado.employee_name.toUpperCase().includes(filtroOK.toUpperCase())){
            console.log(empleado.employee_name);
            pintarTabla(empleado);

        }
    });  
    //document.getElementById("texto_buscado").value = "";
    document.querySelector('parte-uno').shadowRoot.querySelector('#texto_buscado').value = "";

       
}
obtenerDatos();


